import pygame
import sys
from random import randrange
from pygame.locals import *

# Criando constantes para cores
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREN = (0, 255, 0)
ORANGE = (255, 165, 0)

FPS = 5
# inicializando o pygame
pygame.init()

width = 800
height = 600

window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Meu jogo")
timer = pygame.time.Clock()

font_style = pygame.font.SysFont(None, 40)
font_point = pygame.font.SysFont(None, 20)


def message_lost(message, color):
    text = font_style.render(message, True, color)
    window.blit(text, [width / 4, height / 2])


def message_point(point, color):
    point_text = font_point.render(f'Pontos: {point}', True, color)
    window.blit(point_text, [0, 0])


def game_start():
    # Definindo posições iniciais
    x = width / 2
    y = height / 2
    # Criando variáveis para o deslocamento das posições
    delta_x = 0
    delta_y = 0

    block = 10

    food_x = round(randrange(0, width - block) / block) * block
    food_y = round(randrange(0, height - block) / block) * block

    list_block = []
    length_list_block = 1

    game_over = False
    game_lost = False

    # Loop do jogo
    while not game_over:

        while game_lost:
            window.fill(GREN)
            message_lost("Você perdeu aperte c-continuar ou s-sair", RED)
            message_point(length_list_block - 1, ORANGE)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_s:
                        game_over = True
                        game_lost = False
                    if event.key == K_c:
                        game_start()

        # Verificando a entrada
        for event in pygame.event.get():
            if event.type == QUIT:
                game_over = True
            # Verificando se uma tecla foi clicada
            if event.type == KEYDOWN:
                if event.key == K_LEFT:
                    delta_x = -block
                    delta_y = 0
                elif event.key == K_RIGHT:
                    delta_x = block
                    delta_y = 0
                elif event.key == K_UP:
                    delta_x = 0
                    delta_y = -block
                elif event.key == K_DOWN:
                    delta_x = 0
                    delta_y = block

        # Definindo a cor de fundo da janela
        window.fill(WHITE)

        # Verificando se tocou as extremidades da tela
        if x < 0 or x >= width or y < 0 or y >= height:
            game_lost = True

        x += delta_x
        y += delta_y

        # Criando um retângulo
        pygame.draw.rect(window, GREN, [food_x, food_y, block, block])
        block_head = []
        block_head.append(x)
        block_head.append(y)
        list_block.append(block_head)

        # verificando se foi adicionando um bloco na cabeça sem haver o toque
        if len(list_block) > length_list_block:
            del list_block[0]

        # desenhando na tela a sequência de blocos
        for xy in list_block:
            pygame.draw.rect(window, BLACK, [xy[0], xy[1], block, block])

        # Verificando se os blocos agrupados tocam em se mesmo
        for xy in list_block[:-1]:
            if xy == block_head:
                game_lost = True

        # Verificando se um objeto tocou no outro
        if x == food_x and y == food_y:
            food_x = round(randrange(0, width - block) / block) * block
            food_y = round(randrange(0, height - block) / block) * block
            length_list_block += 1

        message_point(length_list_block - 1, ORANGE)
        # Atualizando a tela
        pygame.display.update()
        timer.tick(FPS)

    pygame.quit()
    sys.exit()


if __name__ == "__main__":
    game_start()
